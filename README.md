> # Tráfico Digital - Use and Deployment 
> [![N|Solid](https://www.galvan.com/wp-content/uploads/2016/05/logo_gag_temp021.png)](https://www.galvan.com/)
>
>
>   ## Config venv 🐳 
>   1. Install
>       * ``` python3 -m venv venv ```
>   2. Activate
>       * ``` source venv/bin/activate ```
>   3. Install
>       * ``` pip install wheel ```
>   4. Deactivate
>       * ``` deactivate ```
>   ## Getting started
>   !. Install pyqt5 interfaz
>       * ``` sudo apt-get install qtcreator pyqt5-dev-tools ```
>   2. Open interfaz
>       * ``` designer ```
>   3. Export code
>       * ``` pyuic5 -x ventana.ui -o ventana_ui.py ```
>   ## Get librarys
>   1. Getter'
>       * ``` pip freeze > requirements.txt ```
>   1. Download
>       * ``` pip install -r requirements.txt ```
>   ## Developers 👨‍💻🔥 
>   * **Luis Ortiz @Drowlex** - *Development and Documentation* - [lpedro](https://github.com/Drowlex)
>
>   🎉👨‍💻 _¡Sin miedo al éxito!_ 👨‍💻🎉    